Crenga
=================

Crenga is a simple crawler for Twenga.

How to use it
-----------------
### Simple crawl

Just go on the [Crenga webpage](http://crenga.sitioweb.fr/), and click on the "Crawl Twenga".

The twenga webpage is crawled (it can take some time, a progress bar is displayed if you are on a browser supporting HTML5).

At the end, the webpage is automatically reloaded, and the result list is displayed.

### Dislayed results

A table showing the important information is displayed (id, name, uri, price, stock, etc.).

A "Reliability" column shows us how the "quantity" or "stock" information has been found : 

* Sure : the website has a special configuration
* Guessed : we guessed the information as the webpage has one of the known class on id
* Not on site : we were not able to find any stock information on the webpage
* Not found : we can not determined if the product is in stock on not. We have to ho improve the crawler

### Crawl a product detail page
To trigger the crawler on one product, you can press the "re-crawl" button on the top right on the product line.


Improve the crawler
-----------------
### The GuessCrawler

A "guessing" crawler can find automatically if the product is in stock or not. He is based on some CSS Selectors like '#quantityAvailable' or '.in-stock'.
If a node is found, we update the product. Some generic selectors can be add to improve it.

### Website Specific Crawler

For specific website, we can create a specific class by website. See more in the "Behind the crawler" section.


Behind the crawler
-----------------
The project is build with the [Symfony framework](http://symfony.com).

### Product Manager
The product manager is defined as a Symfony Service of the application and work fully with [Doctrine](http://www.doctrine-project.org) ORM to store the product in MySQL.

### Displaying results
The results component is a Symfony Bundle.
It uses [Twitter Bootstrap](http://twitter.github.com/bootstrap/) stylesheets and [JQuery](http://jquery.com) Javascript library.
It calls the Product Manager to get the product list and displays them with [Twig](http://twig.sensiolabs.org/).

### Crawler
The Crawler is another Symfony Service. It uses the [Goutte](https://github.com/fabpot/Goutte)

The entry point is a factory, witch return a ListCrawlerInterface or a Product\ProductCrawlerInterface.

In our case, we first return a TwengaCrawler (which implements ListCrawlerInterface).

The "doCrawl" method crawls the Twenga website, and hydrate an Product Entity with the id, uniqueId, name and price informations. After that, it has to find the product detail uri by converting twenga's attributes (twenga does not display a "a href" node).
Then it calls back the factory for every product. An instance of Product\ProductCrawlerInterface has to scan the product detail webpage to determine the product stock.


Parameters
-----------------
There are three main parameters visible in "app/config/parameters.yml" : 

* twenga_crawl_uri: 'http://vaisselle.twenga.fr/theiere.html'
* crawler_factory.class: 'Sitioweb\Bundle\CrawlerBundle\Crawler\Factory'
* product_manager.class: 'Sitioweb\Bundle\ProductBundle\Manager'

The services configuration is in the "app/config/config.yml" file.

