<?php

namespace Sitioweb\Bundle\DisplayProductBundle\Controller;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Sitioweb\Bundle\CrawlerBundle\Crawler\TwengaCrawler;

/**
 * DefaultController
 * 
 * @author Julien Deniau <julien.deniau@gmail.com>
 * @Route("/crawler")
 */
class CrawlerController extends Controller
{
    /**
     * session
     * 
     * @var Session
     * @access private
     */
    private $session;

    /**
     * __construct
     *
     * @access public
     * @return void
     */
    public function __construct ()
    {
        //$this->session = new Session();
    }
    
    /**
     * @Route("/twenga", name="crawler_twenga")
     */
    public function crawlTwengaAction ()
    {
        // event 
        $dispatcher = new EventDispatcher();
        $dispatcher->addListener('sitioweb.crawler.twenga', array($this, 'onTwengaUpdate'));
        xcache_unset('sitioweb.crawler.twenga_status');

        //initialize crawler
        $uri = $this->container->getParameter('twenga_crawl_uri');
        $crawler = $this->get('crawler_factory')->getListCrawler('twenga');
        $crawler->setEventDispatcher($dispatcher);

        $crawler->setUri($uri);
        $crawler->doCrawl();

        return new Response('crawl ok');
        //return $this->redirect($this->generateUrl('homepage'));
    }

    /**
     * onTwengaUpdate
     *
     * @param Event $event
     * @access public
     * @return void
     */
    public function onTwengaUpdate(Event $event)
    {
        $status = (int) xcache_get('sitioweb.crawler.twenga_status');
        xcache_set('sitioweb.crawler.twenga_status', $status + 1);

        // saving into the database
        $this->get('doctrine')->getManager()->flush();
        
    }

    /**
     * twengaCheckProgressAction
     *
     * @Route("/twengaCheckProgress", name="crawler_twenga_check_progress")
     * @access public
     * @return void
     */
    public function twengaCheckProgressAction()
    {
        if (xcache_isset('sitioweb.crawler.twenga_status')) {
            $status = (int) xcache_get('sitioweb.crawler.twenga_status');
        } else {
            $status = 0;
        }

        return new Response($status);
    }

    /**
     * resetAction
     *
     * @Route("/reset", name="crawler_reset")
     * @access public
     * @return void
     */
    public function resetAction ()
    {
        $this->get('product_manager')->clearAllProducts();
        return $this->redirect($this->generateUrl('homepage'));
    }

    /**
     * crawlDetailAction
     *
     * @access public
     * @return void
     * @Route("/detail/{uniqId}", name="crawler_detail")
     */
    public function crawlDetailAction ($uniqId)
    {
        $product = $this->get('product_manager')->findProductByUniqId($uniqId);

        $uri = $product->getUri();
        $request = Request::create($uri);
        $host = $request->headers->get('host');

        $crawler = $this->get('crawler_factory')->getDetailCrawler($host);

        $crawler->setUri($uri);
        $crawler->setProduct($product);
        $crawler->doCrawl();

        // saving into the database
        $this->get('doctrine')->getManager()->flush();

        return $this->redirect($this->generateUrl('homepage'));
    }
}

