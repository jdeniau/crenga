<?php

namespace Sitioweb\Bundle\DisplayProductBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * DefaultController
 * 
 * @author Julien Deniau <julien.deniau@gmail.com>
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction()
    {
        $order = $this->get('request')->query->get('order');
        
        $productManager = $this->get('product_manager');
        return array('productList' => $productManager->findProductList($order));
    }
}
