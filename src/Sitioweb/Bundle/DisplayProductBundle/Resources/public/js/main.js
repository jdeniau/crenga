$(document).ready(function() {
    checkProgress= function () {
        $.ajax({
            url : '/crawler/twengaCheckProgress',
            data: {
                cache: new Date().getTime()
            },
            success: function (data) {
                $('#progressBar').val(parseInt(data));
            }
        });
    }

    $(document).on('click', '#crawlTwenga', function () {
        $('#progressBar').val(0);
        $('#buttonBar').hide();
        $('#progressContainer').show();
        var checkProgressInterval = setInterval(checkProgress, 1000);

        $.ajax({
            url : '/crawler/twenga',
            data: {
                cache: new Date().getTime()
            },
            success: function (data) {
                $('#progressBar').val(100);
                $('#buttonBar').show();
                $('#progressContainer').hide();
                clearInterval(checkProgressInterval);
                window.location.reload();
            }
        });
        return false;
    });
});
