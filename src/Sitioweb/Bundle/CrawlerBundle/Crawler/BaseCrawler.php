<?php

namespace Sitioweb\Bundle\CrawlerBundle\Crawler;

use Sitioweb\Bundle\ProductBundle\ManagerInterface;

abstract class BaseCrawler
{
    /**
     * uri
     * 
     * @var string
     * @access private
     */
    private $uri;

    /**
     * productManager
     * 
     * @var ManagerInterface
     * @access private
     */
    private $productManager;


    /**
     * getUri
     *
     * @access public
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * setUri set the uri to crawl
     *
     * @param string $uri
     * @access public
     * @return CrawlerInterface
     */
    public function setUri($uri) {
        $this->uri = $uri;
        return $this;
    }

    /**
     * setProductManager
     *
     * @param ManagerInterface $manager product manager
     * @access public
     * @return CrawlerInterface
     */
    public function setProductManager(ManagerInterface $manager)
    {
        $this->productManager = $manager;
        return $this;
    }

    /**
     * Gets the value of productManager
     *
     * @return ManagerInterface
     */
    public function getProductManager()
    {
        return $this->productManager;
    }
    
    /**
     * doCrawl let's do the crawl
     *
     * @access public
     * @return void
     */
    public abstract function doCrawl();
}

