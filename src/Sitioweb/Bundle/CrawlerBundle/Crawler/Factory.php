<?php

namespace Sitioweb\Bundle\CrawlerBundle\Crawler;
use Sitioweb\Bundle\CrawlerBundle\Exception\Exception;

/**
 * Factory
 * 
 * @author Julien Deniau <julien.deniau@gmail.com>
 */
class Factory
{
    private $productManager;

    /**
     * __construct
     *
     * @param mixed $productManager
     * @access public
     * @return void
     */
    public function __construct($productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * getDetailCrawler
     *
     * @param string $hostname crawler page hostname
     * @access public
     * @return ListCrawlerInterface
     */
    public function getDetailCrawler($hostname)
    {
        $hostId = $this->canonicalize($hostname);
        

        switch ($hostId) {
            case 'tableetambiancefr' :
            case 'villatechfr' :
            case 'cafesmisseguecom' :
                $className = '\\Sitioweb\\Bundle\\CrawlerBundle\\Crawler\\Product\\' . ucfirst($hostId) . 'Crawler';
                
                return new $className();
                break;
            default:
                return new Product\GuessCrawler();
                break;
        }

        if (!isset($crawler)) {
            throw new Exception('Crawler for host "' . $hostId . '" not found');
        }

        $crawler->setProductManager($this->productManager);

        if ($crawler instanceof ListCrawlerInterface) {
            $crawler->setCrawlerFactory($this);
        }

        return $crawler;
    }

    /**
     * getListCrawler
     *
     * @param string $hostname crawler page hostname
     * @access public
     * @return CrawlerInterface
     */
    public function getListCrawler($hostname)
    {
        $hostId = $this->canonicalize($hostname);

        switch ($hostId) {
            case 'twengacom':
            case 'twengafr':
            case 'twenga':
                $crawler = new TwengaCrawler();
                break;
            default:
                break;
        }

        if (!isset($crawler)) {
            throw new Exception('Crawler for host "' . $hostId . '" not found');
        }

        if (!($crawler instanceof ListCrawlerInterface)) {
            throw new Exception('List crawler must implements ListCrawlerInterface');
        }

        $crawler->setProductManager($this->productManager);
        $crawler->setCrawlerFactory($this);

        return $crawler;
    }

    /**
     * canonicalize
     *
     * @param string $hostname
     * @access public
     * @return string
     */
    public function canonicalize($hostname)
    {
        return preg_replace('/[^a-zA-Z]/', '', str_replace('www.', '', $hostname));
    }
}

