<?php

namespace Sitioweb\Bundle\CrawlerBundle\Crawler;

use Sitioweb\Bundle\CrawlerBundle\Exception\MissingParameterException;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\DomCrawler\Link;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Goutte\Client;

class TwengaCrawler extends BaseCrawler implements ListCrawlerInterface
{
    /**
     * crawlerFactory
     * 
     * @var Factory
     * @access private
     */
    private $crawlerFactory;

    /**
     * client
     * 
     * @var Symfony\Component\BrowserKit\Client
     * @access private
     */
    private $client;

    /**
     * eventDispatcher
     * 
     * @var mixed
     * @access private
     */
    private $eventDispatcher;

    /**
     * Gets the value of crawlerFactory
     *
     * @return Factory
     */
    public function getCrawlerFactory()
    {
        return $this->crawlerFactory;
    }
    
    /**
     * Sets the value of crawlerFactory
     *
     * @param Factory $crawlerFactory 
     */
    public function setCrawlerFactory(Factory $crawlerFactory)
    {
        $this->crawlerFactory = $crawlerFactory;
        return $this;
    }

    /**
     * Gets the value of eventDispatcher
     *
     * @return 
     */
    public function getEventDispatcher()
    {
        return $this->eventDispatcher;
    }
    
    /**
     * Sets the value of eventDispatcher
     *
     * @param mixed $eventDispatcher 
     */
    public function setEventDispatcher($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
        return $this;
    }
    
    /**
     * doCrawl let's do the crawl
     *
     * @access public
     * @return ?
     */
    public function doCrawl() {
        $uri = $this->getUri();
        if (empty($uri)) {
            throw new MissingParameterException('There is no specified URI');
        }

        $productManager = $this->getProductManager();
        if (empty($productManager)) {
            throw new MissingParameterException('There is no product manager');
        }

        // create the crawler and call the uri
        $this->client = new Client();
        $crawler = $this->client->request('GET', $uri);

        $this->getEventDispatcher()->dispatch('sitioweb.crawler.twenga');
        
        // getting the list
        $resultList = $this->getResultList($crawler);
        $this->getEventDispatcher()->dispatch('sitioweb.crawler.twenga');

        // treat the first 10 products
        for ($i = 0; $i < 10; $i++) {
            $result = $resultList->eq($i);
            $product = $this->generateProduct($result);

            $this->getEventDispatcher()->dispatch('sitioweb.crawler.twenga');
            
            $detailCrawler = $this->getDetailCrawler($product);

            $this->getEventDispatcher()->dispatch('sitioweb.crawler.twenga');

            try {
                $detailCrawler->doCrawl();
            } catch (MissingParameterException $e) {
                continue;
            }

            $this->getEventDispatcher()->dispatch('sitioweb.crawler.twenga');
        }
    }

    /**
     * getResultList
     *
     * @param Crawler $crawler
     * @access protected
     * @return array<DomElement>
     */
    protected function getResultList (Crawler $crawler)
    {
        return $crawler->filter('#result li');
    }

    /**
     * getDetailCrawler
     *
     * @param Sitioweb\ProductBundle\Entity\Product $product
     * @access protected
     * @return void
     */
    protected function getDetailCrawler(\Sitioweb\Bundle\ProductBundle\Entity\Product $product)
    {
        $request = Request::create($product->getUri());
        $host = $request->headers->get('host');

        $crawler = $this->getCrawlerFactory()->getDetailCrawler($host);
        $crawler->setUri($product->getUri());
        $crawler->setProduct($product);

        return $crawler;
        
    }

    /**
     * generateProduct
     *
     * @param Crawler $result
     * @access protected
     * @return Product
     */
    protected function generateProduct (Crawler $result)
    {
        $manager = $this->getProductManager();

        $productId = $result->attr('data-uniqid');

        // getting the product by uniqId
        $product = $manager->findProductByUniqId($productId);
        if (!$product) {
            // creating a new product if not found
            $product = $manager->createProduct();
            $product->setUniqId($productId);
        }

        // hydrating the object
        $productName = $result->filter('.pdtInfos h2')->text();
        $product->setName($this->transcode($productName));

        $match = array();
        preg_match('/[^0-9]*([0-9\.,]*)/', $result->filter('.pdtBuy .price')->text(), $match);
        if (!empty($match[1])) {
            $product->setPrice((float) str_replace(',', '.', $match[1]));
        }

        // investigate further for the final website
        $sellerUri = $this->getSellerUri($result);
        $product->setUri($sellerUri);

        return $product;
    }

    /**
     * transcode
     * Don't know right now why we need iso-8859-1 here, everything is in UTF-8, need to investigate
     *
     * @param string $string
     * @access private
     * @return void
     */
    private function transcode ($string)
    {
        return utf8_decode($string);
    }

    /**
     * decodeFinalUri
     *
     * @param string $uri
     * @access private
     * @return void
     */
    private function decodeFinalUri($uri)
    {
        $decodedUri = '';
        $uri = str_split($uri);
        foreach ($uri as $c) {
            $ascii = ord($c);
            
            if ($ascii >= 65 && $ascii <= 90) {
                // Twenga transcoding number is 13, 
                // maybe we can find this value automatically with the domain name, because if it change, we have to regenerate it
                // throw an exception somewhere if the uri is invalid
                $ascii += 13;
                if ($ascii > 90) {
                    $ascii -= 26;
                }
            } elseif ($ascii >= 97 && $ascii <= 122) {
                $ascii += 13;
                if ($ascii > 122) {
                    $ascii -= 26;
                }
            }
            $decodedUri .= chr($ascii);
        }
        return $decodedUri;
    }

    /**
     * getSellerUri
     *
     * @param mixed $result
     * @access private
     * @return void
     */
    private function getSellerUri($result)
    {
        $spanLink = $result->filter('.a.oneLk');
        $uri = $spanLink->attr('data-erl');
        $uri = $this->decodeFinalUri($uri);
        $uri .= '&' . $result->filter('.a.oneLk')->attr('data-arg');

        $currentUri = $uri;
        $finalUri = '';
        while ($finalUri === '') {
            try {
                $currentUriHeader = get_headers($currentUri, 1);
                if (!empty($currentUriHeader['Location'])) {
                    // it is a redirection ?
                    if (is_array($currentUriHeader['Location'])) {
                        $currentUri = array_pop($currentUriHeader['Location']);
                    } else {
                        $currentUri = (string) $currentUriHeader['Location'];
                    }
                } elseif (preg_match('/^http:\/\/([^\.]*\.)?twenga\.fr/', $currentUri) ||
                    preg_match('/^http:\/\/([^\.]*\.)?kelkoo\.fr/', $currentUri) ||
                    preg_match('/^http:\/\/([^\.]*\.)?shopzilla\.fr/', $currentUri)) {
                    // damn, we are still on twenga

                    if (preg_match('/^http:\/\/([^\.]*\.)?shopzilla\.fr/', $currentUri)) {
                        $match = array();
                        preg_match('/t=([^&]+)&/', $currentUri, $match);
                        if (!empty($match[1])) {
                            $currentUri = urldecode($match[1]);
                            continue;
                        }
                    }

                    // this page has a 200 code
                    $secondCrawler = $this->client->request('GET', $currentUri);
                    $match = array();
                    preg_match('/location\.href\s=\s"([^"]*)"/', $secondCrawler->text(), $match);
                    if (!empty($match[1])) {
                        $currentUri = $match[1];
                        continue;
                    }

                    // crawler not working here : let's regexp
                    $match = array();
                    $content = $this->client->getResponse()->getContent();
                    $content = substr($content, strripos($content, '<body'));
                    $secondCrawler = new Crawler($content);
                    
                    $active = $secondCrawler->filter('li.active a');
                    
                    if ($active->count() > 0) {
                        $currentUri = $active->attr('href');
                        continue;
                    }

                    $finalUri = $currentUri;
                    break;
                } else {
                    $finalUri = $currentUri;
                }
            } catch (\Exception $e) {
            }
        }

        return $finalUri;
    }
            
}
