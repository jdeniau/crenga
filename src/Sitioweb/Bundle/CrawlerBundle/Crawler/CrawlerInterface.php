<?php

namespace Sitioweb\Bundle\CrawlerBundle\Crawler;

/**
 * CrawlerInterface
 * 
 * @author Julien Deniau <julien.deniau@gmail.com>
 */
interface CrawlerInterface
{
    /**
     * setUri set the uri to crawl
     *
     * @param string $uri
     * @access public
     * @return CrawlerInterface
     */
    public function setUri($uri);

    /**
     * setProductManager
     *
     * @param \Sitioweb\Bundle\ProductBundle\ManagerInterface $manager product manager
     * @access public
     * @return CrawlerInterface
     */
    public function setProductManager(\Sitioweb\Bundle\ProductBundle\ManagerInterface $manager);
    
    /**
     * doCrawl let's do the crawl
     *
     * @access public
     * @return ?
     */
    public function doCrawl();
}

