<?php

namespace Sitioweb\Bundle\CrawlerBundle\Crawler\Product;

use Symfony\Component\DomCrawler\Crawler;
use Sitioweb\Bundle\CrawlerBundle\Crawler\BaseCrawler;
use Sitioweb\Bundle\CrawlerBundle\Crawler\Product\ProductCrawlerInterface;
use Sitioweb\Bundle\CrawlerBundle\Exception\MissingParameterException;
use Goutte\Client;

abstract class BaseProductCrawler extends BaseCrawler implements ProductCrawlerInterface
{
    /**
     * product
     * 
     * @var Product
     * @access private
     */
    private $product;

    /**
     * setProduct
     *
     * @param \Sitioweb\Bundle\ProductBundle\Entity\Product $product
     * @access public
     * @return void
     */
    public function setProduct(\Sitioweb\Bundle\ProductBundle\Entity\Product $product)
    {
        $this->product = $product;
    }

    /**
     * getProduct
     *
     * @access public
     * @return \Sitioweb\Bundle\ProductBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @inherited
     * @see BaseCrawler
     */
    public function doCrawl()
    {
        $uri = $this->getUri();
        
        if (empty($uri)) {
            throw new MissingParameterException('There is no specified URI');
        }

        $product = $this->getProduct();
        if (empty($product)) {
            throw new MissingParameterException('There is no product specified');
        }

        // create the crawler and call the uri
        $this->client = new Client();
        $crawler = $this->client->request('GET', $uri);

        $this->guessStock($crawler);
        
    }

    /**
     * guessStock
     *
     * @param Crawler $crawler
     * @abstract
     * @access protected
     * @return void
     */
    abstract protected function guessStock(Crawler $crawler);
}

