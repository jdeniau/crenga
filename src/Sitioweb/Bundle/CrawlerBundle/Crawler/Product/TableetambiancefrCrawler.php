<?php

namespace Sitioweb\Bundle\CrawlerBundle\Crawler\Product;

use \Symfony\Component\DomCrawler\Crawler;
use Sitioweb\Bundle\ProductBundle\Entity\Product;

/**
 * TableetambiancefrCrawler
 * 
 * @uses BaseProductCrawler
 * @uses ProductCrawlerInterface
 * @author Julien Deniau <julien.deniau@gmail.com>
 */
class TableetambiancefrCrawler extends BaseProductCrawler implements ProductCrawlerInterface
{
    private $filter = '.product-info-box .enstock';

    /**
     * guessStock
     *
     * @inherited
     */
    protected function guessStock(Crawler $crawler)
    {
        $filter = $crawler->filter($this->filter);
        if ($filter->count() > 0) {
            foreach ($filter as $node) {
                if (stripos($node->nodeValue, 'En stock') !== false) {
                    $this->getProduct()->setInStock(true)
                                        ->setReliability(Product::RELIABILITY_SURE);
                }
            }
        }
        return false;
    }
}

