<?php

namespace Sitioweb\Bundle\CrawlerBundle\Crawler\Product;

use \Symfony\Component\DomCrawler\Crawler;
use Sitioweb\Bundle\ProductBundle\Entity\Product;

/**
 * CafesmisseguecomCrawler
 * 
 * @uses BaseProductCrawler
 * @uses ProductCrawlerInterface
 * @author Julien Deniau <julien.deniau@gmail.com>
 */
class CafesmisseguecomCrawler extends BaseProductCrawler implements ProductCrawlerInterface
{
    private $filter = '#fiche_article_stock .mod_fa_stock_oui';

    /**
     * guessStock
     *
     * @inherited
     */
    protected function guessStock(Crawler $crawler)
    {
        $filter = $crawler->filter($this->filter);
        if ($filter->count() > 0) {
            $this->getProduct()->setInStock(true)
                                ->setReliability(Product::RELIABILITY_SURE);
        }
        return false;
    }
}


