<?php

namespace Sitioweb\Bundle\CrawlerBundle\Crawler\Product;

use Sitioweb\Bundle\CrawlerBundle\Crawler\CrawlerInterface;

interface ProductCrawlerInterface extends CrawlerInterface
{
    /**
     * setProduct
     *
     * @param \Sitioweb\Bundle\ProductBundle\Entity\Product $product
     * @access public
     * @return void
     */
    public function setProduct(\Sitioweb\Bundle\ProductBundle\Entity\Product $product);
}

