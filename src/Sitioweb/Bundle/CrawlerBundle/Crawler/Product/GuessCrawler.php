<?php

namespace Sitioweb\Bundle\CrawlerBundle\Crawler\Product;

use \Symfony\Component\DomCrawler\Crawler;
use Sitioweb\Bundle\ProductBundle\Entity\Product;

/**
 * GuessCrawler
 * 
 * @uses BaseProductCrawler
 * @uses ProductCrawlerInterface
 * @author Julien Deniau <julien.deniau@gmail.com>
 */
class GuessCrawler extends BaseProductCrawler implements ProductCrawlerInterface
{

    /**
     * guessStock
     *
     * @inherited
     */
    protected function guessStock(Crawler $crawler)
    {
        // try to find the exact quantity first
        $quantityFilterList = array (
            '#quantityAvailable'
        );

        foreach ($quantityFilterList as $quantityFilter) {
            $quantity = $crawler->filter($quantityFilter);
            
            if ($quantity->count() == 1 && (int) $quantity->text() > 0) {
                $this->getProduct()->setQuantity((int) $quantity->text())
                                    ->setReliability(Product::RELIABILITY_GUESSED);
                return true;
            }
        }

        // try to find stock
        $stockFilterList = array (
            '.availability .in-stock' => true,
            '.stockAvailable' => true,
            '.availGreen' => true,
            '.stock_ok' => true,
            '.enstock' => true,
            '.enStock' => true,
            '#enStock' => true,
            '.en-stock' => true,
            '.instock' => true,
            '.in-stock' => true,
            '.inStock' => true,
            '.inReappro' => false,
            '.inreappro' => false,
            '.in-reappro' => false,
        );

        foreach ($stockFilterList as $stockFilter => $inStock) {
            $stock = $crawler->filter($stockFilter);
            if ($stock->count() == 1) {
                $this->getProduct()->setInStock($inStock)
                                    ->setReliability(Product::RELIABILITY_GUESSED);
                return true;
            }
        }

        // last chance : word stock not found in the page source
        if (!preg_match('/\sstock\s/i', $crawler->text())) {
                $this->getProduct()->setReliability(Product::RELIABILITY_NOT_ON_SITE);
                return true;
        }
        

        return false;
    }
}

