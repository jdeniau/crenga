<?php

namespace Sitioweb\Bundle\CrawlerBundle\Crawler;

/**
 * ListCrawlerInterface
 * 
 * @uses CrawlerInterface
 * @author Julien Deniau <julien.deniau@gmail.com>
 */
interface ListCrawlerInterface extends CrawlerInterface
{
    /**
     * setCrawlerFactory
     *
     * @param  $crawlerFactory
     * @access public
     * @return void
     */
    public function setCrawlerFactory(Factory $crawlerFactory);
}


