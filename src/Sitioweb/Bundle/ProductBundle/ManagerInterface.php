<?php

namespace Sitioweb\Bundle\ProductBundle;

interface ManagerInterface
{
    /**
     * findProductList
     *
     * @param string $order
     * @access public
     * @return array<Product>
     */
    public function findProductList($order);

    /**
     * findProductByUniqId
     *
     * @param int $uniqId
     * @access public
     * @return Product
     */
    public function findProductByUniqId($uniqId);

    /**
     * createProduct
     *
     * @access public
     * @return Product
     */
    public function createProduct();

    /**
     * clearAllProducts
     *
     * @access public
     * @return void
     */
    public function clearAllProducts();
}

