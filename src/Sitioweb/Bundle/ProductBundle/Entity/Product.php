<?php

namespace Sitioweb\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sitioweb\Bundle\ProductBundle\Entity\Product
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Product
{
    const RELIABILITY_SURE = 1;
    const RELIABILITY_GUESSED = 2;
    const RELIABILITY_NOT_FOUND = 3;
    const RELIABILITY_NOT_ON_SITE = 4;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="uniqId", type="bigint")
     */
    private $uniqId;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var float $price
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @var string $uri
     *
     * @ORM\Column(name="uri", type="string", length=1024, nullable=true)
     */
    private $uri;

    /**
     * @var int $quantity
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;

    /**
     * @var boolean $inStock
     *
     * @ORM\Column(name="inStock", type="boolean")
     */
    private $inStock;

    /**
     * @var integer $reliability
     *
     * @ORM\Column(name="reliability", type="smallint")
     */
    private $reliability;

    public function __construct ()
    {
        $this->setReliability(self::RELIABILITY_NOT_FOUND);
        $this->setInStock(false);
        $this->setQuantity(0);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the value of uniqId
     *
     * @return int
     */
    public function getUniqId()
    {
        return $this->uniqId;
    }
    
    /**
     * Sets the value of uniqId
     *
     * @param int $uniqId uniqId 
     */
    public function setUniqId($uniqId)
    {
        $this->uniqId = $uniqId;
        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set uri
     *
     * @param string $uri
     * @return Product
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    
        return $this;
    }

    /**
     * Get uri
     *
     * @return string 
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Gets the quantity
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
    
    /**
     * Sets the quantity
     *
     * @param int $quantity quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        $this->setInStock($quantity > 0);
        return $this;
    }

    /**
     * Set inStock
     *
     * @param boolean $inStock
     * @return Product
     */
    public function setInStock($inStock)
    {
        $this->inStock = $inStock;
    
        return $this;
    }

    /**
     * Get inStock
     *
     * @return boolean 
     */
    public function getInStock()
    {
        return $this->inStock;
    }

    /**
     * Set reliability
     *
     * @param integer $reliability
     * @return Product
     */
    public function setReliability($reliability)
    {
        $this->reliability = $reliability;
    
        return $this;
    }

    /**
     * Get reliability
     *
     * @return integer 
     */
    public function getReliability()
    {
        return $this->reliability;
    }
}
