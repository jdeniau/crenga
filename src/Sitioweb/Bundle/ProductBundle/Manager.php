<?php

namespace Sitioweb\Bundle\ProductBundle;

use \Sitioweb\Bundle\ProductBundle\Entity\Product;

class Manager implements ManagerInterface
{
    /**
     * orm
     * 
     * @var \Doctrine\Bundle\DoctrineBundle
     * @access private
     */
    private $orm;

    /**
     * __construct
     *
     * @param \Doctrine\Bundle\DoctrineBundle $doctrine
     * @access public
     * @return void
     */
    public function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine)
    {
        $this->orm = $doctrine;
    }

    /**
     * findProductList
     *
     * @param string $order
     * @access public
     * @return array<Sitioweb\ProductBundle\Entity\Product>
     */
    public function findProductList($order)
    {
        if (!empty($order)) {
            $order = array($order => 'ASC');
        } else {
            $order = array();
        }
        return $this->orm->getRepository('Sitioweb\Bundle\ProductBundle\Entity\Product')->findBy(array(), $order);
    }

    /**
     * findProductByUniqId
     *
     * @param int $uniqId
     * @access public
     * @return Product
     */
    public function findProductByUniqId($uniqId)
    {
        return $this->orm->getRepository('Sitioweb\Bundle\ProductBundle\Entity\Product')->findOneBy(array('uniqId' => (int) $uniqId));
    }

    /**
     * createProduct
     *
     * @access public
     * @return Product
     */
    public function createProduct()
    {
        $product = new Product();
        $this->orm->getManager()->persist($product);
        return $product;
    }

    /**
     * clearAllProducts
     *
     * @access public
     * @return void
     */
    public function clearAllProducts()
    {
		$query = $this->orm->getManager()->createQuery("DELETE FROM \Sitioweb\Bundle\ProductBundle\Entity\Product");
		$userEventList = $query->execute();
    }
}

